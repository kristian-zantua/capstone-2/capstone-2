const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors')

const PORT = process.env.PORT || 4444;
const app = express();



//Middle ware to handle JSON payloads
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

//Connect database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});


//TEST DB CONNECTION
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => console.log(`Connected to Database`))
  // we're connected!

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')
app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);
app.use(`/api/orders`, orderRoutes);



app.listen(PORT, ()=> console.log(`Server connected to port ${PORT}`))