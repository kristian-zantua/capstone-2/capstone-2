const Product = require('../models/Product');

//create/add product
module.exports.create = async (reqBody) => {
	const {name, description, price} = reqBody

	let newProduct = new Product({
		name: name,
		description: description,
		price: price
        
	})

	return await newProduct.save().then((result, err) => result ? result : err)
}

//get all products
module.exports.getAllProducts = async () => {

	//console.log(decode(req.headers.authorization.id))

        return await Product.find().then(result => result);
	
    }

//get a specific product
module.exports.getAProduct = async (id) => {

	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Products not found`}
			}else{
				return err
			}
		}
	})
}    

//udpate a product
module.exports.updateProduct = async (productId, reqBody) => {
	try{
	return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
} catch(err){console.log(err)}}

//archive product
module.exports.archive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new:true}).then(result => result)
}