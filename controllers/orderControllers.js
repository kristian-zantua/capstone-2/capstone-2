const Order = require('../models/Order');
const Product = require('../models/Product');



//get all orders
module.exports.getAllOrders = async () => {
    return await Order.find().then(result => result)
}

//create/add order
module.exports.createOrder = async (reqBody) => {

    const {userId, totalAmount, products} = reqBody


 
	let newOrder = new Order({
		
        userId: userId,
        totalAmount: totalAmount,
        products: products
        
	});
    

	 await newOrder.save().then((result, err) => result ? console.log(result) : err);
        //console.log(newOrder._id)
        let newOrderId = newOrder._id
        let productId = newOrder.products[0].productId;
        Product.findById(productId).then(result => {

            let orders = {
                orderId: newOrderId,
                quantity: newOrder.products[0].quantity
            }
            result.orders.push(orders)
            result.save().then(result => console.log(result))

        })
        console.log(productId)

        return "order successfully created"
     
    
}