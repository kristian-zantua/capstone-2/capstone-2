const CryptoJS = require("crypto-js");
const User = require('../models/User')
const {createToken} = require("../auth");

//REGISTER A USER
module.exports.register = async (reqBody) => {
    //console.log(reqBody)
    const {firstName, lastName, email, password} = reqBody
    
    const newUser = new User({
        firstName:firstName,
        lastName:lastName,
        email:email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
        
    })
    return await newUser.save().then(result =>{
        if(result){
            return (result)
        } else {
            if(result == null){
                return false
            }
        }
    })
}

//GET ALL USERS
module.exports.getAllUsers = async () => {

    return await User.find().then(result => result)
}

//LOGIN A USER
module.exports.login = async (reqBody) => {
    
    return await User.findOne({email: reqBody.email}).then((result, err) =>{
        if(result == null){
            return {message: `User does not exist.`}
        } else {
            if (result !== null){
                //check if pw is correct
                const decryptPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8);

                console.log(reqBody.password == decryptPw)

                if(reqBody.password == decryptPw){
                    //create a token for the user
                    return {token: createToken(result)}
                } else{
                    return {auth: `auth failed`}
                }
            } else {
                return err
            }
        }
    }) 
}
//Set User as admin
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err);
    
}

