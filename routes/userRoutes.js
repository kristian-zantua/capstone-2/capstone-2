const express = require('express')
const router = express.Router();


//Import units of function
const {

register,
login,
adminStatus,
getAllUsers
} = require('../controllers/userControllers')

const {verify, decode, verifyAdmin} = require('./../auth');


//Register User
router.post('/register', async (req, res)=> {
    //console.log(req.body)
    try{
    await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

//Get all users

router.get('/', async(req, res) => {
    try{
        await getAllUsers().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//LOGIN THE USER
router.post('/login', (req, res) => {
    try {
        login(req.body).then(result => res.send(result))
    }catch(err){
         res.status(500).json(err)
    }
})
//Set User as admin
router.patch('/isAdmin', verifyAdmin, async (req, res) => {
    try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

module.exports = router;