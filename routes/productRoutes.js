const express = require(`express`);
const router = express.Router();

const {
    create,
    getAllProducts,
    getAProduct,
	updateProduct,
	archive

} = require('../controllers/productControllers')

const { verify, decode, verifyAdmin} = require('./../auth')

//create.add product
router.post('/create', verifyAdmin, async (req, res) => {
	// console.log(req.body)
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//get all products
router.get('/allproducts', verify, async (req, res) => {
	try{
		await getAllProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

//get a specific product
router.get('/:productId', verify, async (req, res) => {
	try{
		await getAProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//update product information
router.put('/:productId/update', verifyAdmin, async (req, res) => {
    try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

//archive product
router.patch('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




module.exports = router;