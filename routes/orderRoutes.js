const express = require(`express`);
const router = express.Router();
//const jwt = require('jsonwebtoken');

const {
    
    getAllOrders,
	createOrder
} = require('../controllers/orderControllers')

const {verifyAdmin, verify} = require('./../auth')

//get all orders
router.get('/allorders', verify, async (req, res) => {

	try{
		await getAllOrders().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})
//create order
router.post('/checkout', verify, async (req, res) => {
	//const isAdmin = jwt.decode(req.headers.authorization).isAdmin
	//console.log(isAdmin)
	try{
		await createOrder(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


module.exports = router;